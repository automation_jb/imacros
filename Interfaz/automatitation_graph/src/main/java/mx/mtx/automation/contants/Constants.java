/*
 * @(#)Constants.java
 *
 * Copyright (c) 2016 Southwest Airlines, Co.
 * 2702 Love Field Drive, Dallas, TX 75235, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Southwest Airlines, Co.
 */

package mx.mtx.automation.contants;

/**
 * @author x222559 Oscar Mejia
 *
 */
public class Constants {
   
   public static final String DESCRIPTION_SCENARIO_1="Create an order BORROW type and with po_lines (BO and MISC)";
   public static final String DESCRIPTION_SCENARIO_2="Create an order PURCHASE type and with po_lines (PO and MISC)";
   public static final String DESCRIPTION_SCENARIO_3="Create an order EXCHANGE type and with po_lines (EX and MISC)";
   public static final String DESCRIPTION_SCENARIO_4="Create an order REPAIR type and with po_lines (RO and MISC)";
   public static final String DESCRIPTION_SCENARIO_5="Update an order BORROW type and with po_lines (BO and MISC) - update quantity";
   public static final String DESCRIPTION_SCENARIO_6="Actualización de una order tipo PURCHASE con po_lines (PO y MISC) - Actualize unit cost";
   public static final String DESCRIPTION_SCENARIO_7="Actualización de una order tipo EXCHANGE con po_lines (EX y MISC) - Actualize account code";
   public static final String DESCRIPTION_SCENARIO_8="Actualización de una order tipo REPAIR con po_lines (RO y MISC) - Actualize new line";
   public static final String DESCRIPTION_SCENARIO_9="Cancelación de una order tipo BORROW con po_lines (BO y MISC)";
   public static final String DESCRIPTION_SCENARIO_10="Cancelación  de una order tipo PURCHASE con po_lines (PO y MISC)";
   public static final String DESCRIPTION_SCENARIO_11="Cancelación  de una order tipo EXCHANGE con po_lines (EX y MISC)";
   public static final String DESCRIPTION_SCENARIO_12="Cancelación  de una order tipo REPAIR con po_lines (RO y MISC)";
   public static final String DESCRIPTION_SCENARIO_13="Convertir una order tipo EXCHANGE a PURCHASE";
   public static final String DESCRIPTION_SCENARIO_14="Convertir una order tipo REPAIR  a EXCHANGE";
   public static final String DESCRIPTION_SCENARIO_15="Convertir una order tipo BORROW a PURCHASE";
   public static final String DESCRIPTION_SCENARIO_16="Convertir una order tipo BORROW a EXCHANGE";
   

}

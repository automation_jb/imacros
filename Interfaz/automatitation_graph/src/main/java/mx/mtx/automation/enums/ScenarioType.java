/*
 * @(#)ScenarioType.java
 *
 * Copyright (c) 2016 Southwest Airlines, Co.
 * 2702 Love Field Drive, Dallas, TX 75235, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Southwest Airlines, Co.
 */

package mx.mtx.automation.enums;

import java.util.ArrayList;
import java.util.List;

import mx.mtx.automation.contants.Constants;

/**
 * @author x222559 Oscar Mejia
 *
 */
public enum ScenarioType {
   
   SCENARIO_0(0,"Select Scenario","Select Scenario"),
   SCENARIO_1(1,"Scenario 1",Constants.DESCRIPTION_SCENARIO_1),
   SCENARIO_2(2,"Scenario 2",Constants.DESCRIPTION_SCENARIO_2),
   SCENARIO_3(3,"Scenario 3",Constants.DESCRIPTION_SCENARIO_3),
   SCENARIO_4(4,"Scenario 4",Constants.DESCRIPTION_SCENARIO_4),
   SCENARIO_5(5,"Scenario 5",Constants.DESCRIPTION_SCENARIO_5),
   SCENARIO_6(6,"Scenario 6",Constants.DESCRIPTION_SCENARIO_6),
   SCENARIO_7(7,"Scenario 7",Constants.DESCRIPTION_SCENARIO_7),
   SCENARIO_8(8,"Scenario 8",Constants.DESCRIPTION_SCENARIO_8),
   SCENARIO_9(9,"Scenario 9",Constants.DESCRIPTION_SCENARIO_9),
   SCENARIO_10(10,"Scenario 10",Constants.DESCRIPTION_SCENARIO_10),
   SCENARIO_11(11,"Scenario 11",Constants.DESCRIPTION_SCENARIO_11),
   SCENARIO_12(12,"Scenario 12",Constants.DESCRIPTION_SCENARIO_12),
   SCENARIO_13(13,"Scenario 13",Constants.DESCRIPTION_SCENARIO_13),
   SCENARIO_14(14,"Scenario 14",Constants.DESCRIPTION_SCENARIO_14),
   SCENARIO_15(15,"Scenario 15",Constants.DESCRIPTION_SCENARIO_15),
   SCENARIO_16(16,"Scenario 16",Constants.DESCRIPTION_SCENARIO_16);
   
   public String identificator;
   public int id;
   public String description;
   
   ScenarioType(int id,String identificator, String description){
      this.id=id;
      this.identificator=identificator;
      this.description=description;
   }

   /**
    * @return the identificator
    */
   public String getIdentificator() {
      return identificator;
   }

   /**
    * @return the id
    */
   public int getId() {
      return id;
   }


   /**
    * @return the description
    */
   public String getDescription() {
      return description;
   }

   public String toString(){
      return description;
   }
   
   public static String[] descriptionValues(){
      List<String> descriptions =  new ArrayList<String>();
      ScenarioType[] scenarios=ScenarioType.values();
      for(ScenarioType scenarioType : scenarios){
         descriptions.add(scenarioType.description);
      }
      
      String[] descriptionsStrings = (String[])descriptions.toArray(new String[16]);  
      return  descriptionsStrings;
   }
   

}

/*
 * @(#)AutomationGUI.java
 *
 * Copyright (c) 2016 Southwest Airlines, Co.
 * 2702 Love Field Drive, Dallas, TX 75235, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Southwest Airlines, Co.
 */

package mx.mtx.automation.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import com.globant.imacros.automation.Principal;

import mx.mtx.automation.dto.ScenarioToRun;
import mx.mtx.automation.enums.ScenarioType;

/**
 * @author x222559 Oscar Mejia
 *
 */
public class AutomationGUI {

   private JFrame frmAutomationTestMxi;
   private JTextField textFieldRepetitions;
   private JComboBox<ScenarioType> comboBoxScenarios ;
   JTextArea textAreaScenariosToRun;

   private ScenarioType scenarioSelected;
   public List<ScenarioToRun> listScenariosToRun = new ArrayList<ScenarioToRun>();

   JLabel lblErrorHandling = new JLabel("");
   private int reptitions = 0;

   /**
    * Launch the application.
    */
   public static void main(String[] args) {
      EventQueue.invokeLater(new Runnable() {
         public void run() {
            try {
               AutomationGUI window = new AutomationGUI();
               Toolkit tk = Toolkit.getDefaultToolkit();
               int xSize = ((int) tk.getScreenSize().getWidth());
               int ySize = ((int) tk.getScreenSize().getHeight());
               window.frmAutomationTestMxi.setSize(xSize, ySize);
               window.frmAutomationTestMxi.setVisible(true);
            } catch (Exception e) {
               e.printStackTrace();
            }
         }
      });
   }

   /**
    * Create the application.
    */
   public AutomationGUI() {
      initialize();
   }

   /**
    * Initialize the contents of the frame.
    */
   private void initialize() {
      frmAutomationTestMxi = new JFrame();
      frmAutomationTestMxi.setTitle("Automation Test MXI");
      frmAutomationTestMxi.setBounds(0, 0, 747, 445);
      frmAutomationTestMxi.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      comboBoxScenarios = new JComboBox<ScenarioType>();
      comboBoxScenarios.setModel(new DefaultComboBoxModel<ScenarioType>(ScenarioType.values()));
      comboBoxScenarios.setToolTipText("Select Scenarios to run");

      textAreaScenariosToRun = new JTextArea();
      textAreaScenariosToRun.setEditable(false);
      textAreaScenariosToRun.setLineWrap(true);
      JLabel lblScneario = new JLabel("Scenario:");

      textFieldRepetitions = new JTextField();
      textFieldRepetitions.setColumns(5);

      JLabel lblRepetitions = new JLabel("No Lines:");

      JLabel lblSelectedScenarios = new JLabel("Selected Scenarios :");

      JButton btnExceute = new JButton("Execute");
      btnExceute.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
           //TODO invoke method to run test
            Principal p = new Principal();
            
            for(ScenarioToRun scenario : listScenariosToRun){
               System.out.println("Scenario: " + scenario.getScenarioType().description + " " + scenario.getRepetitions());
               try {
                  p.ensamblarScript(scenario.getScenarioType().id, scenario.getRepetitions());
               } catch (IOException e1) {
                  e1.printStackTrace();
               }
            }
            
            cleanFields();
            
         }
      });

      JButton btnClean = new JButton("Clean");
      btnClean.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
           cleanFields();
         }
      });

      JSeparator separator = new JSeparator();

      JLabel lblPleaseSelectThe = new JLabel("Please Select the Scenarios to Run");
      lblPleaseSelectThe.setFont(new Font("Tahoma", Font.BOLD, 11));

      JButton btnAddScenarioTo = new JButton("Add Scenario to Run");
      btnAddScenarioTo.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {

            scenarioSelected = (ScenarioType) comboBoxScenarios.getSelectedItem();
            if (scenarioSelected.equals(ScenarioType.SCENARIO_0)) {
               JOptionPane.showMessageDialog(null, "Pelase select a valid scenario.", "Error",
                     JOptionPane.WARNING_MESSAGE);
               return;
            } 
            try {
               reptitions = new Integer(textFieldRepetitions.getText()).intValue();
            } catch (NumberFormatException ne) {
               lblErrorHandling.setForeground(Color.RED);
               lblErrorHandling.setText("Repetitions number is wrong");
               return;
            }
            ScenarioToRun scenario = new ScenarioToRun();
            scenario.setRepetitions(reptitions);
            scenario.setScenarioType(scenarioSelected);
            listScenariosToRun.add(scenario);
            lblErrorHandling.setForeground(Color.BLUE);
            lblErrorHandling.setText("Scenario added");
            textAreaScenariosToRun.setFont(new Font("Tahoma", Font.BOLD, 14));
            textAreaScenariosToRun.setForeground(Color.BLUE);
            textAreaScenariosToRun.append("|"+scenarioSelected.getDescription() + " |  Repetitions: " + reptitions + "|\t" + "\n");
            comboBoxScenarios.setSelectedIndex(0);
            textFieldRepetitions.setText("");
            textAreaScenariosToRun.repaint();
         }
      });

      JScrollPane scrollPane = new JScrollPane();
      scrollPane.setViewportBorder(new LineBorder(new Color(0, 0, 0)));
      scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
      scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

      lblErrorHandling.setFont(new Font("Tahoma", Font.BOLD, 14));
      lblErrorHandling.setForeground(Color.RED);
      GroupLayout groupLayout = new GroupLayout(frmAutomationTestMxi.getContentPane());
      groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(groupLayout.createSequentialGroup()
                  .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                              .addGap(150)
                                 .addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(lblRepetitions)
                                       .addComponent(lblScneario)
                                 )
                              .addPreferredGap(ComponentPlacement.UNRELATED)
                                 .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                       .addGroup(groupLayout.createSequentialGroup()
                                             .addComponent(textFieldRepetitions, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
                                             .addGap(32).addComponent(btnAddScenarioTo)
                                       )
                                       .addComponent(comboBoxScenarios, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
                                 )
                         )
                        .addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(separator,GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE))
                        .addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(lblSelectedScenarios))
                        .addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(scrollPane,GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE)))
                  .addContainerGap()
             )
            .addGroup(groupLayout.createSequentialGroup().addGap(26).addComponent(lblPleaseSelectThe).addPreferredGap(ComponentPlacement.RELATED, 292, Short.MAX_VALUE)
                  .addComponent(lblErrorHandling).addGap(216))
            .addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup().addContainerGap(342, Short.MAX_VALUE).addComponent(btnClean).addGap(35).addComponent(btnExceute).addGap(228)));
      groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap()
            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblPleaseSelectThe).addComponent(lblErrorHandling)).addGap(28)
               .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(comboBoxScenarios, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
                  .addComponent(lblScneario)).addGap(18)
                     .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
                           .addGroup(groupLayout.createSequentialGroup()
                                 .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                       .addComponent(textFieldRepetitions, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,GroupLayout.PREFERRED_SIZE)
                                       .addComponent(lblRepetitions))
                                 .addGap(23))
                           .addGroup(groupLayout.createSequentialGroup().addComponent(btnAddScenarioTo)
                                 .addPreferredGap(ComponentPlacement.RELATED)))
                     .addGap(48)
                     .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                     .addPreferredGap(ComponentPlacement.UNRELATED).addComponent(lblSelectedScenarios)
                     .addPreferredGap(ComponentPlacement.RELATED)
                     .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
                     .addPreferredGap(ComponentPlacement.UNRELATED)
                     .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(btnExceute).addComponent(btnClean))
            .addContainerGap()));

      scrollPane.setViewportView(textAreaScenariosToRun);

      groupLayout.setAutoCreateGaps(true);
      groupLayout.setAutoCreateContainerGaps(true);
      frmAutomationTestMxi.getContentPane().setLayout(groupLayout);
   }
   
   private void cleanFields(){
      textFieldRepetitions.setText("");
      comboBoxScenarios.setSelectedIndex(0);
      textAreaScenariosToRun.setText("");
      if(!listScenariosToRun.isEmpty()){
         listScenariosToRun.clear();
      }
      
   }
}

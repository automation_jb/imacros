/*
 * @(#)ScenarioToRun.java
 *
 * Copyright (c) 2016 Southwest Airlines, Co.
 * 2702 Love Field Drive, Dallas, TX 75235, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Southwest Airlines, Co.
 */

package mx.mtx.automation.dto;

import mx.mtx.automation.enums.ScenarioType;

/**
 * @author x222559 Oscar Mejia
 *
 */
public class ScenarioToRun {
   
   private ScenarioType scenarioType;
   private int repetitions;
   /**
    * @return the scenarioType
    */
   public ScenarioType getScenarioType() {
      return scenarioType;
   }
   /**
    * @param scenarioType the scenarioType to set
    */
   public void setScenarioType(ScenarioType scenarioType) {
      this.scenarioType = scenarioType;
   }
   /**
    * @return the repetitions
    */
   public int getRepetitions() {
      return repetitions;
   }
   /**
    * @param repetitions the repetitions to set
    */
   public void setRepetitions(int repetitions) {
      this.repetitions = repetitions;
   }

}

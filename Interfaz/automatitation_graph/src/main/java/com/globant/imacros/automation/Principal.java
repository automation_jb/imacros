package com.globant.imacros.automation;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

import com.jacob.activeX.ActiveXComponent;

public class Principal {
   
   public static final String UTF8_BOM = "\uFEFF";
   private StringBuffer sb = new StringBuffer();
   private ActiveXComponent iim = new ActiveXComponent("imacros");
   private Boolean called = false;
   private String IMACROS_PATH = "";
   private String MACROS_PATH = "";
   private String URL = "";
   private String OUT_PATH = "";
   private String TYPE;

   
   public Principal() {
      Properties prop = new Properties();
      InputStream input = null;
      
      try {
         input = new FileInputStream("config.properties");
         prop.load(input);

         IMACROS_PATH = prop.getProperty("imacros_path");
         MACROS_PATH = prop.getProperty("macros_subpath");
         URL = prop.getProperty("mxi_url");
         OUT_PATH = prop.getProperty("output_path");
         
      } catch (IOException ex) {
         ex.printStackTrace();
      } finally {
         if (input != null) {
            try {
               input.close();
            } catch (IOException e) {
               e.printStackTrace();
            }
         }
      }
   }
   
   public static void main(String[] args) {
      Principal p = new Principal();
      int actividad = 3;
      int cantidadLineas = 5;
      try {
         p.ensamblarScript(actividad, cantidadLineas);
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   public String createTransformedScript(String pathOriginScript, String salida) throws IOException{
      StringBuffer sb1 = new StringBuffer();
      List<String> lines=Files.readAllLines(Paths.get(pathOriginScript));
      lines.forEach(str->sb1.append(fixLine(str,0)+"\n"));
      String finalName = IMACROS_PATH+MACROS_PATH+salida;
      Files.write(Paths.get(finalName), sb1.toString().getBytes());
      return finalName;
   }
   
 

   public void ensamblarScript(int activity,int amountLines) throws IOException{
      if(!called){
         System.out.println("Calling iimInit");
         iim.invoke("iimInit");
         System.out.println("Calling iimPlay");
         iim.invoke("iimPlay", createTransformedScript(IMACROS_PATH+MACROS_PATH+"LoginMX.iim","temp1.iim"));
         called=true;
      }
      sb = new StringBuffer();
      switch(activity){
         case 1:
            executeActivity(amountLines, "1 create BORROW.iim", "1_1 Create BORROW Line Misc.iim", "1_2 Auth on BORROW Order Misc Line.iim");
            TYPE = "Create";
            break;
         case 2:
            executeActivity(amountLines, "2 CreationPurchaseOrder.iim", "2_1 CreationPurchaseOrder Line Misc.iim", "2_2 Auth on Purchase Order Misc Line.iim");
            TYPE = "Create";
            break;
         case 3:
            executeActivity(amountLines, "3 Create Exchange.iim", "3_1 Create Exchange Line Misc.iim", "3_2 Create Exchange Auth.iim");     
            TYPE = "Create";
            break;
         case 4:
            executeActivity(amountLines, "4.CreateRepair.iim", "4_1 Create Repair Line Misc.iim", "4_2 Auth on Repair Misc Line.iim");
            TYPE = "Create";
            break;
         case 5:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"5 Update BORROW Order Misc Line.iim","temp2.iim"));
            iim.invoke("iimPlay",IMACROS_PATH+MACROS_PATH+"5_1 Auth UpdateBorrowOrder.iim");
            TYPE = "Update";
            break;
         case 6:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"6 UpdatePurchaseOrder.iim","temp2.iim"));
            iim.invoke("iimPlay",IMACROS_PATH+MACROS_PATH+"6_1 Auth UpdatePurchaseOrder.iim");
            TYPE = "Update";
            break;
         case 7:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"7 Update Exchange Order Misc Line.iim","temp2.iim"));
            iim.invoke("iimPlay",IMACROS_PATH+MACROS_PATH+"7_1 Auth on update Exchange Order Misc Line.iim");
            TYPE = "Update";
            break;
         case 8:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"8.EditRepair.iim","temp2.iim"));
            TYPE = "Update";
            break;
         case 9:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"9 Cancel Borrow Order.iim","temp2.iim"));
            TYPE = "Cancel";
            break;
         case 10:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"10 CancelPurchaseOrder.iim","temp2.iim"));
            TYPE = "Cancel";
            break;
         case 11:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"11 Cancel Exchange Order Misc Line.iim","temp2.iim"));
            TYPE = "Cancel";
            break;
         case 12:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"12.CancelRepair.iim","temp2.iim"));
            TYPE = "Cancel";
            break;
         case 13:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"13 ConvertOrderExchangeToPurchase.iim","temp2.iim"));
            TYPE = "Convert";
            break;
         case 14:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"14 Convert Repair to Exchange.iim","temp2.iim"));
            TYPE = "Convert";
            break;
         case 15:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"15.ConvertBorrowToPurchase.iim","temp2.iim"));
            TYPE = "Convert";
            break;
         case 16:
            iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+"16.ConvertBorrowToExchance.iim","temp2.iim"));
            TYPE = "Convert";
            break;
            
      }
      List<String> lines=Files.readAllLines(Paths.get(IMACROS_PATH+MACROS_PATH+"Save MXI XML.iim"));
      lines.forEach(str->sb.append(fixLine(str,0)+"\n"));
      Files.write(Paths.get(IMACROS_PATH+MACROS_PATH+"tempMacroSave.iim"), sb.toString().getBytes());
      iim.invoke("iimPlay", IMACROS_PATH+MACROS_PATH+"tempMacroSave.iim");
      sb = new StringBuffer();
   }
   
   public void executeActivity(int amountLines, String scriptCreateHeader, String scriptCreateLines,String scriptAuthOrder){
      try{
         iim.invoke("iimPlay",createTransformedScript(IMACROS_PATH+MACROS_PATH+scriptCreateHeader,"tempHead.iim"));
         List<String> lines=Files.readAllLines(Paths.get(IMACROS_PATH+MACROS_PATH+scriptCreateLines));
         if(amountLines==1){
            lines.forEach(str->sb.append(fixLine(str,2)+"\n"));
            sb.append("WAIT SECONDS=2\n");
            Files.write(Paths.get(IMACROS_PATH+MACROS_PATH+"tempMacro.iim"), sb.toString().getBytes());
            iim.invoke("iimPlay", IMACROS_PATH+MACROS_PATH+"tempMacro.iim");
            sb=new StringBuffer();
         }else{
            for(int i=0;i<amountLines;i++){
               if(i==0){
                  lines.stream().forEach(str->sb.append(fixLine(str,2)+"\n"));
               }else{
                  for(String linea:lines){
                     sb.append(fixLine(linea,(i+2))+"\n");
                  }
               }
               
               System.out.println(sb.toString());
               sb.append("WAIT SECONDS=2\n");
               Files.write(Paths.get(IMACROS_PATH+MACROS_PATH+"tempMacro.iim"), sb.toString().getBytes());
               iim.invoke("iimPlay", IMACROS_PATH+MACROS_PATH+"tempMacro.iim");
               sb=new StringBuffer();
            }
         }
         
         iim.invoke("iimPlay", createTransformedScript(IMACROS_PATH+MACROS_PATH+scriptAuthOrder,"tempAuth.iim"));
         
      }catch(IOException e){
         e.printStackTrace();
      }
   }
   
   public String fixLine(String line,int consec){
      if(line.startsWith(UTF8_BOM)){
         System.out.println("Si!");
         line = line.substring(1);
      }
      if(line.contains("##NumberLine")){
         line=line.replace("##NumberLine", consec+"");
      }
      if(line.contains("##URL##")){
         line=line.replace("##URL##", URL);
      }
      if(line.contains("##IMACROS_PATH##")){
         line=line.replace("##IMACROS_PATH##", IMACROS_PATH.replace("/", "\\"));
      }
      if(line.contains("##OUTPUT##")){
         line=line.replace("##OUTPUT##", (OUT_PATH+TYPE).replace("/", "\\"));
      }
      return line;
   }
}